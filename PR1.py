import math



def Choose():
    choose = int(input("Чтобы перейти к выбору операций нажмите 1: "))
    if (choose == 1):
        Calculator()
    else:
        print("Некоректный ввод")
        Choose()

def ChooseM():
    choose = int(input("Чтобы перейти к выбору функций нажмите 1: "))
    if (choose == 1):
        Main()
    else:
        print("Некоректный ввод")
        ChooseM()

def select_operation(choice):
    if choice == 1:
        return lambda a, b, y: 2 * (a * b + b * y + a * y)
    elif choice == 2:
        return lambda a, b, y: 4 * a + 4 * b + 4 * y


def Calculator():
    print("1 - сложение\n2 - вычитание\n"
          "3 - деление\n4 - умножение\n5 - деление на цело\n"
          "6 - нахождение остатка от деления\n7 - возведение в степень\n"
          "8 - нахождение корня\n9 - косинус\n10 - синус\n11 - тангенс\n"
          "12 - переход к выбору функций\n13 - расчет площади параллелепипеда \n"
          "14 - расчет периметра параллелепипеда \n")
    try:
        number = int(input("Введите число: "))
    except:
        print("Error")
        Calculator()
    if (number == 1):
        while (True):
            try:
                number1 = int(input("Введите число 1: "))
                number2 = int(input("Введите число 2: "))
                numberSum = number1 + number2
                print(f"Результат: {numberSum}")
                Choose()
            except:
                print("Error")
    elif (number == 2):
        while (True):
            try:
                number1 = int(input("Введите число 1: "))
                number2 = int(input("Введите число 2: "))
                Res = number1 - number2
                print(f"Результат: {Res}")
                Choose()
            except:
                print("Error")

    elif (number == 3):
        while(True):
            try:
                number1 = int(input("Введите число 1: "))
                number2 = int(input("Введите число 2: "))
                Res = number1 / number2
                print(f"Результат: {Res}")
                Choose()
                break
            except:
                print("Error")
    elif (number == 4):
        while (True):
            try:
                number1 = int(input("Введите число 1: "))
                number2 = int(input("Введите число 2: "))
                Res = number1 * number2
                print(f"Результат: {Res}")
                Choose()
            except:
                print("Error")

    elif (number == 5):
        while (True):
            try:
                number1 = int(input("Введите число 1: "))
                number2 = int(input("Введите число 2: "))
                Res = number1 // number2
                print(f"Результат: {Res}")
                Choose()
            except:
                print("Error")

    elif (number == 6):
        while (True):
            try:
                number1 = int(input("Введите число 1: "))
                number2 = int(input("Введите число 2: "))
                Res = number1 % number2
                print(f"Результат: {Res}")
                Choose()
            except:
                print("Error")

    elif (number == 7):
        while (True):
            try:
                number1 = int(input("Введите число 1: "))
                number2 = int(input("Введите число 2: "))
                Res = number1 ** number2
                print(f"Результат: {Res}")
                Choose()
            except:
                print("Error")

    elif (number == 8):
        while (True):
            try:
                number1 = int(input("Введите число: "))
                Res = math.sqrt(number1)
                print(f"Результат: {Res}")
                Choose()
            except:
                print("Error")

    elif (number == 9):
        while (True):
            try:
                number1 = int(input("Введите число: "))
                Res = math.cos(number1)
                print(f"Результат: {Res}")
                Choose()
            except:
                print("Error")

    elif (number == 10):
        while (True):
            try:
                number1 = int(input("Введите число: "))
                Res = math.sin(number1)
                print(f"Результат: {Res}")
                Choose()
            except:
                print("Error")

    elif (number == 11):
        while (True):
            try:
                number1 = int(input("Введите число: "))
                Res = math.tan(number1)
                print(f"Результат: {Res}")
                Choose()
            except:
                print("Error")

    elif(number == 12):
        while (True):
            try:
                Main()
            except:
                print("Error")

    elif (number == 13):
        while (True):
            try:
                operation = select_operation(1)
                numberA = int(input("Введите число a: "))
                numberB = int(input("Введите число b: "))
                numberC = int(input("Введите число c: "))
                print("Результат:", operation(numberA, numberB, numberC))
                Choose()
            except:
                print("Error")

    elif (number == 14):
        while (True):
            try:
                operation = select_operation(2)
                numberA = int(input("Введите число a: "))
                numberB = int(input("Введите число b: "))
                numberC = int(input("Введите число c: "))
                print("Результат:", operation(numberA, numberB, numberC))
                Choose()
            except:
                print("Error")
    else:
        print("Error")
        Calculator()




def Word():
    try:
        a = (input("Введите текст\n"))
        res_symbols = len(a)
        res_probelov = len(a) - len(a.replace(" ", ""))
        start = -1
        res_zyapyatih = 0
        while True:
            start = a.find(',', start + 1)
            if start == -1:
                break
            res_zyapyatih +=1


        print(f"Символов в строке: {res_symbols}")
        print(f"Кол-во пробелов: {res_probelov}")
        print(f"Кол-во запятых: {res_zyapyatih}")
        ChooseM()
    except:
        print("Error")
        Word()

def Matrix():
    while(True):
        try:
            KStolbov = int(input("Укажите кол-во столбцов в матрице: "))
            KStrok = int(input("Укажите кол-во строк в матрице: "))
            Nachalo = int(input("Укажите значение, с которого начинается матрица: "))
            N = Nachalo
            Count = KStolbov
            Yvelichenie = int(input("Укажите шаг матрицы: "))
            while (KStrok > 0):
                while (Count < KStolbov):
                    print(Nachalo, end=" ")
                    Nachalo += Yvelichenie
                    Count += 1
                print(end="\n")
                KStrok -= 1

                Count = 0
            ChooseM()
        except :
            print("Error")
            Matrix()


def Main():
    try:
        print("1 - калькулятор\n2 - подсчет символов в строке\n3 - матрица\n4 - выход")
        choose = int(input("Введите число: "))
        if (choose == 1):
            Calculator()
        elif (choose == 2):
            Word()
        elif (choose == 3):
            Matrix()
        elif (choose == 4):
            SystemExit()
        else:
            print("Некоректный ввод")
            Main()
    except:
        print("Error")
        Main()

Main()